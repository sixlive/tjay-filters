<?php
/**
 * Plugin Name: TJAY Filters
 * Plugin URI: https://tjay.co
 * Description: Custom Filtering
 * Version: 0.0.1
 * Author: TJ Miller
 * Author URI: https://tjay.co
 * License: GPL2
 */


if ( !defined( 'IOF_PLUGIN_URL' ) )
  define( 'IOF_PLUGIN_URL', plugins_url( '', __FILE__ ));
if ( !defined( 'IOF_PLUGIN_DIR' ) )
  define( 'IOF_PLUGIN_DIR', dirname( __FILE__ ) );

$auto_load = array(
  'vendor/autoload.php',
  'class-html.php',
  'class-io-filters-base.php',
  'class-io-filters-ajax.php',
  'class-io-filters-tables.php',
  'class-io-filters-admin.php',
);

if( !empty( $auto_load ) )
  foreach( $auto_load as $load )
    require_once IOF_PLUGIN_DIR . '/includes/' . $load;

class ioFilters extends ioFiltersBase
{

  public function __construct()
  {
    parent::__construct();

    if( is_admin() )
      $this->admin_hooks();
  }

  public function admin_hooks()
  {
    register_activation_hook( __FILE__, array( $this, 'activationHook' ) );

    add_action('init', function() {
      ioFiltersTables::check();
    });

    new ioFiltersAdmin();
    new ioFiltersAjax();
  }

  public function activationHook()
  {
    ioFiltersTables::create();
  }
}

/* -- Init -- */
new ioFilters();