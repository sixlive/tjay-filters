(function($) {

  /**
   * Resets the field values and updates the name index
   * @param  {object} el The cloned element
   * @param  {ing} total The current total of cloned fields
   * @return void
   */
  function reset_fields( el, total ) {
    el.find('input, textarea').each(function() {
      $(this).val('');
      $(this).attr( 'name', $(this).data('name-prefix') + '[' + total + ']' + '[' + $(this).data('name') + ']' );
    });
    el.find('[data-media-preview]').html('');
    el.find('[data-io-media-upload]').show();
    el.find('[data-io-media-remove]').hide();

    var select = el.find('select');
    var selected_default;
    var key = 0;
    el.find('select').children().each(function() {
      if( $(this).data('default-select') != undefined ) {
        selected_default = key;
      }
      key++;
    });

    if( $(selected_default).length ) {
      $(select).prop('selectedIndex', selected_default);
    } else {
      $(select).prop('selectedIndex', 0);
    }
  }

  /**
   * Gets the total of repeated items
   * @param  {object} el The element that fires the count
   * @return void
   */
  function get_total_groups( el ) {
    return el.parents('[data-io-repeat-container]').find('[data-io-repeat-group]').length;
  }

  /**
   * Document ready instance
   */
  $(document).ready(function() {

    /**
     * Removed clone group. Accounts for zero item fallback
     * @return void
     */
    $('[data-io-meta-remove]').live('click', function() {
      var clone_group = $(this).parents('[data-io-repeat-group]');

      var total = get_total_groups( $(this) );

      if( total <= 1 ) {
        $(clone_group).find('[data-io-meta-repeat]').click();
      }

      $(clone_group).remove();
    });

    /**
     * Clones the group and inserts after the current element
     * @return {[type]} [description]
     */
    $('[data-io-meta-repeat]').live('click', function() {
      var clone_group = $(this).parents('[data-io-repeat-group]');
      var clone_container = $(this).parents('.inside').find('[data-io-repeat-container]');
      var total = get_total_groups( $(this) );
      var clone = $(clone_group).clone(true);

      reset_fields(clone, total );
      clone_group.after(clone);
    });

    /**
     * Removes the media element preview and id, resets for new media upload
     * @return void
     */
    $('[data-io-media-remove]').live('click', function() {
      var form_group = $(this).parents('.form-group');

      $(form_group).find('[data-media-id]').val('');
      $(form_group).find('[data-media-preview]').html('');
      $(this).hide();
      $(form_group).find('[data-io-media-upload]').show();
    });

    /**
     * Opens wp media to add imare
     * @return void
     */
    $('[data-io-media-upload]').click(function() {
        var input = this;

        var form_group = $(input).parents('.form-group');

        var old_send_to_editor = window.send_to_editor;

        wp.media.editor.send.attachment = function( props, attachment ) {
          props.size = 'full';
          props = wp.media.string.props( props, attachment );
          props.align = null;

          $(form_group).find('[data-media-id]').val(attachment.id);
          $(form_group).find('[data-media-preview]').html('<img src="' + attachment.url + '" style="max-width:100%;height:auto;">');
          $(input).hide();
          $(form_group).find('[data-io-media-remove]').show();
        };

        wp.media.editor.open( input );
    });
  });

})(jQuery);