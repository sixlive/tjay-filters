@if( !empty( $filters ) )
  <div class="fluid-container">
    <div class="row visible-md visible-lg" style="padding:1em 0">
      <div class="col-md-8"><strong>Filter Name</strong></div>
    </div>

    @foreach( $filters as $filter )
     <div class="row" data-item-id="{{ $filter->id }}">
      <div class="col-sm-7"><div class="hidden-md hidden-lg"><strong>Filter Name:</strong></div>{{ $filter->filter_title }}</div>
      <div class="col-sm-5">
        <span class="pull-right">
          <button type="button" class="btn btn-default btn-sm" data-io-move-up><i class="fa fa-chevron-up"></i></button>
          <button type="button" class="btn btn-default btn-sm" data-io-move-down><i class="fa fa-chevron-down"></i></button>
          <button type="button" title="Edit Item" class="btn btn-warning btn-sm" data-io-edit="{{ $filter->id }}"><i class="fa fa-edit"></i></button>
          <button type="button" title="Remove Item" class="btn btn-danger btn-sm" data-io-remove="{{ $filter->id }}"><i class="fa fa-trash-o" ></i></button>
        </span>
      </div>
    </div>
    @endforeach
  </div>
@else
  <p>No filters available.</p>
@endif