<?php

class ioFiltersAjax extends ioFiltersBase
{

  public function __construct()
  {
    parent::__construct();

    add_action( 'init', [$this, 'init'] );
  }

  public function init()
  {
    // Groups
    add_action( 'wp_ajax_filter_group_listing_display', [$this, 'ajax_filter_group_listing'] );
    add_action( 'wp_ajax_filter_group_add', [$this, 'ajax_filter_group_add'] );
    add_action( 'wp_ajax_filter_group_delete', [$this, 'ajax_filter_group_delete'] );

    // Filters
    add_action( 'wp_ajax_filters_listing', [$this, 'ajax_filters_listing'] );
    add_action( 'wp_ajax_filters_add', [$this, 'ajax_filters_add'] );
    add_action( 'wp_ajax_filters_delete', [$this, 'ajax_filters_delete'] );
  }

  public function ajax_filter_group_listing()
  {
    $groups = $this->wpdb->get_results( "SELECT * FROM {$this->tables['filter_groups']}");

    echo $this->blade()->view()->make( 'ajax.group-listing', ['groups' => $groups] );
    die();
  }

  public function ajax_filter_group_add()
  {
    $data = [];
    parse_str( $_POST['data'], $data );

    $this->wpdb->insert( $this->tables['filter_groups'],
      [
        'post_type' => $data['io_add_group_post_type'],
        'group_title' => $data['io_add_group_title'],
        'group_name' => $this->sanitize_str( $data['io_add_group_title'] )
      ],
      [
        '%s',
        '%s',
        '%s'
      ]
    );

    die();
  }

  public function ajax_filter_group_delete()
  {
    $this->wpdb->delete( $this->tables['filter_groups'], ['id' => $_POST['id']] );
    die();
  }

  public function ajax_filters_listing()
  {
    $filters = $this->wpdb->get_results(
      $this->wpdb->prepare(
        "SELECT * FROM {$this->tables['filters']} WHERE filter_group_id = %d", $_POST['group_id']
    ) );

    echo $this->blade()->view()->make( 'ajax.filters-listing', ['filters' => $filters ] );
    die();
  }

  public function ajax_filters_add()
  {
    $data = [];
    parse_str( $_POST['data'], $data );

    $slug = $this->create_unique_slug( $data['filter_title'], $id = null, $table = $this->tables['filters'], $slug_col = 'filter_name', $id_col = 'id' );

    $this->wpdb->insert( $this->tables['filters'],
      [
        'filter_title' => $data['filter_title'],
        'filter_group_id' => $data['group_id'],
        'filter_name' => $slug,
        'filter_display_order' => $data['display_order']
      ],
      [
        '%s',
        '%d',
        '%s',
        '%d'
      ]
    );

    die();
  }

  public function ajax_filters_delete()
  {
    $this->wpdb->delete( $this->tables['filters'], ['id' => $_POST['id']] );
    die();
  }
}