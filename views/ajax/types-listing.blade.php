<div>
  <div class="clearfix" data-group>
    <div class="controls pull-right">
      <button type="button" class="btn btn-default btn-sm" data-io-move-up><i class="fa fa-chevron-up"></i></button>
      <button type="button" class="btn btn-default btn-sm" data-io-move-down><i class="fa fa-chevron-down"></i></button>
      <button type="button" title="Edit Item" class="btn btn-warning btn-sm"><i class="fa fa-edit"></i></button>
      <button type="button" title="Remove Item" class="btn btn-danger btn-sm" data-io-remove><i class="fa fa-trash-o" ></i></button>
    </div>
  </div>
</div>