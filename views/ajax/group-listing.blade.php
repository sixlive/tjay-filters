@if( !empty( $groups ) )
  <div class="fluid-container">
    <div class="row visible-md visible-lg" style="padding:1em 0">
      <div class="col-md-5"><strong>Group Name</strong></div>
      <div class="col-md-4"><strong>Post Type</strong></div>
    </div>

    @foreach( $groups as $group )
     <div class="row">
      <div class="col-md-5"><div class="hidden-md hidden-lg"><strong>Group Name:</strong></div>{{ $group->group_title }}</div>
      <div class="col-md-4"><div class="hidden-md hidden-lg"><strong>Post Type:</strong></div>{{ $group->post_type }}</div>
      <div class="col-md-3">
        <span class="pull-right">
          <button type="button" class="btn btn-default btn-sm"><i class="fa fa-edit"></i></button>
          <button type="button" class="btn btn-danger btn-sm" data-delete-group="{{ $group->id }}"><i class="fa fa-trash-o"></i></button>
        </span>
      </div>
    </div>
    @endforeach
  </div>
@else
  <p>No groups available.</p>
@endif