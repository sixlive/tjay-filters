@extends( $layout )

@section( 'content' )
  <div class="postbox">
    <h3>Filters</h3>
    <div class="inside clearfix" data-listing>
    </div><!-- inside -->
  </div><!-- postbox -->
@stop

@section( 'sidebar' )
  <div class="postbox">
    <h3>Add Filters</h3>
    <div class="inside">
      <div class="container-fluid">
        <div class="row">
          {{ ioHTML::formOpen('', '', ['data-add-form' => null, 'id' => 'add-group-form'] ); }}

            <div class="form-group">
              {{ ioHTML::input( 'filter_title', '', ['placeholder' => 'Filter Name' ] ) }}
            </div>

            {{ ioHTML::input( 'group_id', $group_id, ['type' => 'hidden', 'data-keep' => null] ) }}

           <div class="form-group">
            <button type="button" class="btn btn-primary btn-block" data-add-btn>Add Filter</button>
           </div>
          {{ ioHTML::formClose(); }}
        </div><!-- row -->
      </div><!-- container-fluid -->
    </div><!-- inside -->
  </div><!-- postbox -->
@stop

@section( 'js' )
  <script>
    jQuery(function($) {

      $(document).on('refresh_listing', function() {
        var listing_container = $('[data-listing]');
        var loader = $($.iof_loading_icon);

        loader.appendTo(listing_container.parents('.postbox').find('h3'));

        listing_container.css('opacity', .5);

        var data = {
          'action': 'filters_listing',
          'group_id': '{{ $group_id }}'
        };

        $.post(ajaxurl, data, function(response) {
          loader.fadeOut();
          listing_container.css('opacity', 1);
          listing_container.html(response);
        });

      }).trigger('refresh_listing');

      $('[data-add-btn]').click(function() {
        var form = $(this).parents('form');
        var form_data = form.serialize();

        var total = function() {
          return $('[data-listing]').find('[data-group]').length;
        };

        // Add initial display order
        form_data += "&display_order=" + encodeURIComponent( total() );

        console.log( form_data );

        var data = {
          'action': 'filters_add',
          'data': form_data,
        };

        $.post(ajaxurl, data, function(response) {
          setTimeout(function() {
            $('[data-add-form]').clear_fields();
          }, 1000);

          $.event.trigger('refresh_listing');
        });
      });

      $('[data-io-remove]').live('click', function() {
        var btn = $(this);
        btn.html($.iof_loading_icon);

        var data ={
          'action': 'filters_delete',
          'id': btn.data('io-remove')
        };

        $.post(ajaxurl, data, function(response) {
          btn.parents('.row').remove();
        });
      });

    });
  </script>
@stop