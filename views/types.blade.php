@extends( $layout )

@section( 'content' )
  <div class="postbox">
    <h3>Types</h3>
    <div class="inside clearfix" data-listing>
    </div><!-- inside -->
  </div><!-- postbox -->
@stop

@section( 'sidebar' )
  <div class="postbox">
    <h3>Add Types</h3>
    <div class="inside">
      <div class="container-fluid">
        <div class="row">
          {{ ioHTML::formOpen('', '', ['data-add-group-form' => null, 'id' => 'add-group-form'] ); }}

            <div class="form-group">
              {{ ioHTML::input( 'io_add_group_title', '', ['placeholder' => 'Group Name' ] ) }}
            </div>

           <div class="form-group">
            <button type="button" class="btn btn-primary btn-block" data-add-group>Add Group</button>
           </div>
          {{ ioHTML::formClose(); }}
        </div><!-- row -->
      </div><!-- container-fluid -->
    </div><!-- inside -->
  </div><!-- postbox -->
@stop

@section( 'js' )
  <script>
  jQuery(function($) {

  });
  </script>
@stop