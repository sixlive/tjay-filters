<?php
class ioFiltersTables extends ioFiltersBase
{

  public static function check()
  {
    global $wpdb;
    $tables_exist = true;

    $iof = new Self();
    $tables = $iof->tables;


    //Exit if tables are created
    foreach( $tables as $table ) {
      if( $wpdb->get_var("SHOW TABLES LIKE '{$table}'") != $table )
        $tables_exist = false;
    }

    if( $tables_exist ) {
      return true;
    } else {
      add_action( 'admin_notices', function() {
        echo '<div id="message" class="error"><p>There is a problem with ioFilter\'s tables. Try de-activating and re-  activating the plugin.</p></div>';
      });
     return false;
    }
  }

  public static function create()
  {
    global $wpdb;
    $sql = array();

    $iof = new Self();
    $tables = $iof->tables;

    $sql[] = "
      CREATE TABLE IF NOT EXISTS {$tables['filters']} (
       `id` int(11) NOT NULL AUTO_INCREMENT,
       `filter_name` varchar(255) NOT NULL,
       `filter_title` varchar(255) NOT NULL,
       `filter_group_id` int(11) NOT NULL,
       `filter_display_order` int(11) NOT NULL,
       PRIMARY KEY (`id`),
       KEY `filter_name` (`filter_name`),
       KEY `filter_group_id` (`filter_group_id`)
      ) ";

    $sql[] = "
      CREATE TABLE IF NOT EXISTS {$tables['types']} (
       `id` int(11) NOT NULL AUTO_INCREMENT,
       `type_name` varchar(255) NOT NULL,
       `type_title` varchar(255) NOT NULL,
       `parent_id` int(11) NOT NULL DEFAULT '0',
       `type_display_order` int(11) NOT NULL,
       PRIMARY KEY (`id`),
       KEY `type_name` (`type_name`),
       KEY `parent_id` (`parent_id`)
      )";

    $sql[] = "
      CREATE TABLE IF NOT EXISTS {$tables['filter_relations']} (
       `type_id` int(11) NOT NULL,
       `filter_id` int(11) NOT NULL
      )";

  $sql[] = "
    CREATE TABLE IF NOT EXISTS {$tables['filter_groups']} (
     `id` int(11) NOT NULL AUTO_INCREMENT,
     `post_type` varchar(128) NOT NULL,
     `group_title` varchar(128) NOT NULL,
     `group_name` varchar(128) NOT NULL,
     PRIMARY KEY (`id`)
    )";

    foreach( $sql as $query )
      $create_table = $wpdb->query( trim( $query ) );
  }
}
?>