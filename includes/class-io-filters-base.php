<?php

class ioFiltersBase
{
  public $asset_uri;
  public $asset_path;
  public $tables;
  public $wpdb;
  public $config = array(
    'tables' => array(
      'filters' => 'iof_filters',
      'types' => 'iof_filter_types',
      'filter_relations' => 'iof_filter_relationships',
      'filter_groups' => 'iof_filter_groups'
    )
  );

  public function __construct()
  {
    global $wpdb;
    $this->wpdb = $wpdb;

    $upload_dir = wp_upload_dir();
    $make_cache_dir = true;

    foreach( $this->config['tables'] as $key => $table )
      $this->config['tables'][$key] = $wpdb->prefix . $table;

    $this->config['asset_uri'] = IOF_PLUGIN_URL . '/assets';
    $this->config['asset_url'] = IOF_PLUGIN_DIR . '/assets';
    $this->view_path = IOF_PLUGIN_DIR . '/views';
    $this->cache_path = $upload_dir['basedir'] . '/io-filters-cache';
    $this->tables = $this->config['tables'];

    if( !file_exists( $this->cache_path ) )
      $make_cache_dir = mkdir( $this->cache_path );

    if( ! $make_cache_dir )
      ioFiltersBase::adminMessage( 'error', 'ioFilters failed to create cache path in the uploads directory, please check permissions and make sure uploads is writable.' );
  }

  public static function adminMessage( $type = '', $message = '' )
  {
    echo '<div id="message" class="'.esc_attr( $type ).'">'.wpautop( $message ).'</div>';
  }

  function sanitize_str( $string )
  {
    $string = trim( $string );
    $string = preg_replace('/\s/', '-', $string);
    $string = preg_replace('/[^A-Za-z0-9\-]/', '-', $string);
    $string = preg_replace('/-+/', '-', $string);

    return strtolower( $string );
  }

  public function blade() {
    return new Philo\Blade\Blade( $this->view_path, $this->cache_path );
  }

  public function create_unique_slug( $string, $id = null, $table = null, $slug_col = null, $id_col = 'id' )
  {
    $sanatize = function( $string ) {
      $slug = trim( $string );
      $slug = preg_replace( '/\s+/', '-', $slug );
      $slug = preg_replace( '/[^A-Za-z0-9\-]/', '-', $slug );
      $slug = preg_replace( '/-+/', '-', $slug );
      $slug = strtolower( $slug );

      return $slug;
    };

    $slug = $sanatize( $string );

    if( !empty( $id ) ) {
      $old_slug = $this->wpdb->get_var( "SELECT {$slug_col} FROM {$table} WHERE {$id_col} = '{$id}'" );

      if( $slug == $old_slug )
        return $old_slug;

    }

    $query = function( $slug_col, $table, $slug ) {
      return $this->wpdb->get_results( "SELECT {$slug_col} FROM {$table} WHERE {$slug_col} = '{$slug}'" );
    };

    if( !empty( $query( $slug_col, $table, $slug ) ) ) {
      $orig_slug = $slug;
      $count = 2;
      while( $query( $slug_col, $table, $slug ) ) {
        $slug = $orig_slug . '-' . $count;
        $count++;
      }
    }

    return $slug;
  }
}