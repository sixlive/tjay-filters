<?php

class ioFiltersAdmin extends ioFiltersBase
{

  public $wpdb;
  public $tables;
  public $pages = [];
  public $pages_groups;

  public function __construct()
  {
    parent::__construct();

    global $wpdb;

    $this->wpdb = $wpdb;
    $this->pages_groups= $this->fetchGroups();

    if( !empty( $_GET['page'] ) && in_array( $_GET['page'], $this->pages ) )
      ob_start();

    add_action( 'admin_menu', array( $this, 'addPages' ) );
  }

  public function cssJs()
  {
    wp_register_style( 'FontAwesome', '//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css' );

    wp_enqueue_style( 'bootstrap-custom', $this->config['asset_uri'] . '/css/bootstrap.min.css' );
    wp_enqueue_style( 'FontAwesome' );
    wp_enqueue_style( 'iofilters', $this->config['asset_uri'] . '/css/io-filters.css' );

    wp_register_script( 'io-clear-fields', $this->config['asset_uri'] . '/js/io-clear-fields.js', ['jquery'] );
    wp_enqueue_script( 'jquery' );
    wp_enqueue_script( 'io-filters', $this->config['asset_uri'] . '/js/io-filters.js', ['jquery'] );
    wp_enqueue_script( 'io-clear-fields' );
    wp_enqueue_script( 'happy-methods', $this->config['asset_uri'] . '/js/happy.methods.js', ['jquery'] );
    wp_enqueue_script( 'happy', $this->config['asset_uri'] . '/js/happy.js', ['jquery', 'happy-methods'] );
  }

  public function fetchGroups()
  {
    $sql = "SELECT * FROM {$this->tables['filter_groups']}";

    return $this->wpdb->get_results( trim( $sql ) );
  }

  public function addPages()
  {

    $capability = 'publish_posts';

    // Main Admin Page
    $this->pages[] = 'io-filters-admin';
    add_menu_page( 'Filters Admin', 'ioFilters', $capability, 'io-filters-admin', array( $this, 'viewGroups' ) );

    if( !empty( $this->pages_groups ) ) {
      $post_types = [];

      foreach( $this->pages_groups as $page ) {
        $post_types[] = $page->post_type;
        $this->pages[] = 'io-filters-'.$page->id;
        add_submenu_page( 'edit.php?post_type='.$page->post_type, ucwords( $page->group_title ) . ' Filters', ucwords( $page->group_title ) . ' Filters', $capability, 'io-filters-'.$page->id, array( $this, 'viewFilters' ) );
        add_submenu_page( 'edit.php?post_type='.$page->post_type, ucwords( $page->group_title ) . ' Types', ucwords( $page->group_title ) . ' Types', $capability, 'io-types-'.$page->id, array( $this, 'viewTypes' ) );
      }
    }
  }

  public function view()
  {
    $this->cssJs();
    return new Philo\Blade\Blade( $this->view_path, $this->cache_path );
  }

  public function viewFilters()
  {
    $blade = $this->view();

    $group_id = $this->stripId( $_GET['page'] );
    $filters = $this->getFilters( $group_id );
    $group = $this->getGroup( $group_id );

    $vars = [
      'page_title' => $group[0]->group_title . ' Filters',
      'layout' => 'layouts.master',
      'group_id' => $group_id
    ];

    echo $blade->view()->make( 'filters', $vars );
  }

  public function stripId( $get_str )
  {
    $match = preg_match( '/\d/', $_GET['page'], $group_id );
    return $group_id[0];
  }

  public function viewTypes()
  {
    $blade = $this->view();

    $group = $this->getGroup( $this->stripId( $_GET['page'] ) );

    $vars = [
      'page_title' => $group[0]->group_title . ' Types',
      'layout' => 'layouts.master'
    ];

    echo $blade->view()->make( 'types', $vars );
  }

  public function getGroup( $group_id )
  {
    $sql = "SELECT * FROM {$this->tables['filter_groups']} WHERE id = {$group_id}";

    return $this->wpdb->get_results( trim( $sql ) );
  }

  public function getFilters( $group_id )
  {
    $sql = "
      SELECT * FROM {$this->tables['filters']}
      WHERE filter_group_id = {$group_id}
    ";

    return $this->wpdb->get_results( trim( $sql ) );
  }

  public function viewGroups()
  {
    $blade = $this->view();

    $post_types = function() {
      $select = [];

      foreach( get_post_types( $args = [ 'public' => true ], 'objects' ) as $slug => $pType ) {
        $select[$slug] = $pType->labels->name;
      }

      return $select;
    };

    $vars = [
      'page_title' => 'ioFilters',
      'layout' => 'layouts.master',
      'post_types' => $post_types()
    ];

    echo $blade->view()->make( 'add-groups', $vars );
  }

  public static function userMessages()
  {
    $type = '';
    $message = '';

    if( !empty( $_GET['io-message'] ) ) {
      switch( $_GET['io-message'] ) {

        case 1:
          $type = 'updated';
          $message = 'Settings Saved.';
        break;

        case 2:
          $type = 'error';
          $message = "No settings changed / error updating settings. Please verify settings below.";
        break;
      }
    }

    return '<div id="message" class="'.esc_attr( $type ).'">'.apply_filters( 'the_content', $message ).'</div>';
  }
}
?>