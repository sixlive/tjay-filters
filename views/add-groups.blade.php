@extends( $layout )

@section( 'styles' )
  <style>
    [data-group-listing] .row {
      padding-top: .5em;
      margin-bottom: .5em;
      border-top: 1px solid #c9c9c9;
    }

    [data-group-listing] .row:first-child {
      padding-top: 0;
      border-top: none
    }
  </style>
@stop

@section( 'content' )

  <div class="postbox">
    <h3>Groups</h3>
    <div class="inside clearfix" data-group-listing>
    </div><!-- inside -->
  </div><!-- postbox -->

@stop

@section( 'sidebar' )
  <div class="postbox">
    <h3>Add Groups</h3>
    <div class="inside">
      <div class="container-fluid">
        <div class="row">
          {{ ioHTML::formOpen('', '', ['data-add-group-form' => null, 'id' => 'add-group-form'] ); }}

            <div class="form-group">
              {{ ioHTML::input( 'io_add_group_title', '', ['placeholder' => 'Group Name' ] ) }}
            </div>

            <div class="form-group">
              {{ ioHTML::select( 'io_add_group_post_type', $post_types, '', ['select_placeholder' => 'Choose post type', 'form' => 'add-group-form' ]  ) }}
           </div>

           <div class="form-group">
            <button type="button" class="btn btn-primary btn-block" data-add-group>Add Group</button>
           </div>
          {{ ioHTML::formClose(); }}
        </div><!-- row -->
      </div><!-- container-fluid -->
    </div><!-- inside -->
  </div><!-- postbox -->
@stop

@section( 'js' )
  <script>
    jQuery(function($) {

      $(document).on('refresh_groups', function() {
        var group_listing = $('[data-group-listing]');
        var group_loading = $($.iof_loading_icon);

        group_loading.appendTo(group_listing.parents('.postbox').find('h3'));
        group_listing.css('opacity', .5);
        $.post(ajaxurl, {'action':'filter_group_listing_display'}, function(response) {
          group_loading.fadeOut();
          group_listing.css('opacity', 1);
          group_listing.html(response);
        });

      }).trigger('refresh_groups');

      $('[data-add-group]').click(function() {
        var form = $(this).parents('form');
        var form_data = form.serialize();

        var data = {
          'action': 'filter_group_add',
          'data': form_data
        };

        $.post(ajaxurl, data, function(response) {
          setTimeout(function() {
            $('[data-add-group-form]').clear_fields();
          }, 1000);

          $.event.trigger('refresh_groups');
        });
      });

      $('[data-delete-group]').live('click', function() {
        var btn = $(this);
        btn.html($.iof_loading_icon);

        var data ={
          'action': 'filter_group_delete',
          'id': btn.data('delete-group')
        };

        $.post(ajaxurl, data, function(response) {
          btn.parents('.row').remove();
        });
      });

    });
  </script>
@stop